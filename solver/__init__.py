from scipy.sparse import csr_matrix as Matrix
from numpy import ndarray as Vector, append

# TODO: re-export other classes/functions you want to be part of your package.
#       For instance, if you have a file _my_class.py in this folder, in which
#       a class MyClass is defined, you can write
#
#       from ._my_class import MyClass
#
#       This enables users to write `from solver import MyClass`

# HINT: scipy.sparse provides the spsolve function that you can use
#       to solve the linear system after you have assembled it. Simply
#       import it with
#           from scipy.sparse.linalg import spsolve
#       and invoke it with a matrix and right-hand-side (can be a numpy ndarray):
#           rhs: numpy.ndarray = ...
#           solution: numpy.ndarray = spsolve(matrix, rhs)


# TODO: Remove once generic grid interface is available
class SquareGrid:
    """
        Dummy implementation of a structured square grid that can be used for initial solver development.
        Should be removed once a generic grid interface is available and the solver can be generalized.
    """
    def __init__(self, size: float, num_cells_per_dir: int) -> None:
        self._dx = size/float(num_cells_per_dir)
        self._num_cells_per_dir = num_cells_per_dir

    @property
    def dx(self) -> float:
        return self._dx

    @property
    def number_of_cells_per_direction(self) -> int:
        return self._num_cells_per_dir

    @property
    def number_of_cells(self) -> int:
        return self._num_cells_per_dir*self._num_cells_per_dir

    def index(self, i: int, j: int) -> int:
        """Return a unique index for the cell at location (i, j)"""
        return j*self._num_cells_per_dir + i

    def domain_position(self, i: float | int, j: float | int) -> tuple[float, float]:
        """Return the domain coordinates of the location (i, j) in the raster"""
        return (
            self.dx*(float(i) + 0.5),
            self.dx*(float(j) + 0.5)
        )


# TODO: generalize once generic grid interface is available
def _make_sparse_matrix(grid: SquareGrid) -> Matrix:
    """
    Create a sparse matrix with the sparsity pattern of the TPFA discretization scheme for the given grid
    """
    row: Vector = Vector(shape=(0,))
    col: Vector = Vector(shape=(0,))
    data: Vector = Vector(shape=(0,))
    for j in range(grid.number_of_cells_per_direction):
        for i in range(grid.number_of_cells_per_direction):
            cell_index = grid.index(i, j)
            col_indices = [cell_index]
            col_indices.extend(_get_neighbor_indices(grid, i, j))
            for col_idx in col_indices:
                row = append(row, cell_index)
                col = append(col, col_idx)
                data = append(data, 0.0)
    return Matrix(
        (data, (row, col)),
        shape=(grid.number_of_cells, grid.number_of_cells)
    )


# TODO: should become obsolete once generic grid interface is available
def _get_neighbor_indices(grid: SquareGrid, i: int, j: int) -> list[int]:
    """
    Return the indices of the neighboring cells that are within the stencil of the given cell
    """
    cell_index = grid.index(i, j)
    neighbors = []
    if j > 0:
        neighbors.append(cell_index - grid.number_of_cells_per_direction)
    if i > 0:
        neighbors.append(cell_index - 1)
    if j < grid.number_of_cells_per_direction - 1:
        neighbors.append(cell_index + grid.number_of_cells_per_direction)
    if i < grid.number_of_cells_per_direction - 1:
        neighbors.append(cell_index + 1)
    return neighbors
