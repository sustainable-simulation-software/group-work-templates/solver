This is a template repository for the `solver` package to be developed in a student project within the
course "Sustainable Development of Simulation Software". For a full description of the project, see
[here][projectreadme]. This template repository predefines a folder structure for the project as well as
a basic [CI](https://docs.gitlab.com/ee/ci/) that automatically executes the tests, lints the code with
[flake8](https://flake8.pycqa.org/en/latest/) and does type checking with [mypy](https://mypy-lang.org/).
To execute these tasks in the command line, move to the top folder of this repository and type:

```bash
python -m pytest
flake8
mypy solver
```

__Note__: Replace this `README` with an actual description of the project during development.


Requirements
============

As outlined in the [project description][projectreadme], we want to solve diffusion problems on arbitrary 2D or 3D grids.
Grid implementations will be provided in the [grid](https://gitlab.com/sustainable-simulation-software/group-work-templates/grid)
package and this `solver` package should be interoperable with the grid interface developed therein. For a given grid, we want
to be able to solve a diffusion problem for:

- user-defined boundary conditions (per boundary face)
- user-defined diffusion coefficients (per grid cell)
- arbitrary grids (fulfilling the required grid interface)


Development suggestions
=======================

In the beginning of the project, the `grid` package is not yet available. Therefore, it is recommended to start with developing a simplified
solver, hardcoded for structured square grids, Dirichlet boundary conditions on all boundaries and $\Phi = 1$. Let us consider a structured
2d grid with dimensions $X \times X$ and $N \times N$ cells. This yields a discretization length of $\mathrm{d}x = X/N$. Let us identify the
cells in the grid by index tuples $(i, j)$, where $i$ is the column and $j$ the row index in the structured grid. For cells that do not touch
the boundary, on this setting the discrete equation (see the last equation in the
[project description](https://gitlab.com/sustainable-simulation-software/course-material/-/tree/main/project#discretization-scheme))
can be simplified to:

```math
- u_{i-1, j} - u_{i+1, j} - u_{i, j-1} - u_{i, j+1} + 4 u_i = q_{i, j}*\mathrm{d}x*\mathrm{d}x.
```

For cells that touch (Dirichlet) boundaries, one has to incorporate the boundary fluxes $F_b = - \mathrm{d}x \frac{u_b - u_{i, j}}{\mathrm{d}x/2} = - 2(u_b - u_{i, j})$.
As an example, for a cell that touches (only) the left boundary, the discrete equation reads:

```math
- 2 u_{i-0.5, j} - u_{i+1, j} - u_{i, j-1} - u_{i, j+1} + 5 u_i = q_{i, j}*\mathrm{d}x*\mathrm{d}x,
```

where $u_{i-0.5, j}$ refers to the Dirichlet boundary condition on the left boundary at row $j$.


### Suggested steps:

- test your implementation of the above with a hardcoded analytical solution (see [project description](https://gitlab.com/sustainable-simulation-software/course-material/-/tree/main/project#discretization-scheme)) by means of a convergence test (see `test/test_convergence.py`)
- allow injection of the boundary condition values and source term by the user (and test convergence for different analytical solutions)
- allow users to inject a custom parameter $\Phi$
- generalize implementation once the generic [grid](https://gitlab.com/sustainable-simulation-software/group-work-templates/grid) interface is available, in particular, position-dependent $\Phi$ should now be possible
- Verify that the expected convergence rates are obtained also for rectangular grids


[projectreadme]: https://gitlab.com/sustainable-simulation-software/course-material/-/tree/main/project
