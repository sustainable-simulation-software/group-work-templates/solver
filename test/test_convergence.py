# TODO: import your class/function to be tested
# from solver import ???

# TODO: Maybe you want to unit-test code before reaching the convergence test?

def test_convergence():
    # TODO: solve a problem with known analytical solution for a series
    #       of refined structured, square grids. On each refinement,
    #       compute an error norm between the discrete and the exact solution after
    #       e = dx*dx*\sum_i (u_{exact} - u_i)^2,
    #       where dx is the discretization length of the grid, u_i is the solution at
    #       the center of cell i and u_{exact} is the exact solution at the center of
    #       cell i.
    #       Compute the convergence rate between two refinements with
    #       r = (log(e_k) - log(e_{k-1})) / (log(dx_k) - log(dx_{k-1})),
    #       where e_k is the error norm for refinement k.
    #       We should get convergence rates of approximately 2!
    assert False
